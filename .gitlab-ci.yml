## DEFAULT ENV
# HARBOR_ADDRESS: harbor.dieunkrauts.de
# HARBOR_PROJECT: project1

stages:
  - build-push-container-images
  - build-helm-charts
  - push-helm-charts

# Job for building and pushing container images (Repo: /docker)
build_and_push_container_images:
  image: gcr.io/kaniko-project/executor:debug
  stage: build-push-container-images

  # This script iterates through the /docker repo
  # It sets the sub-repo as context and catches the Dockerfile 
  # The tag is set to the image | when provided, the commit tag is set - on main the latest tag is set
  # After setting all the options, the /kaniko/executor builds and pushes the image to harbor
  # The auth-credentials are stored in the runner
  script:
    - |
      for docker_dir in $CI_PROJECT_DIR/docker/*/ ; do
        IMAGE_DIR=$docker_dir/Dockerfile
        IMAGE_NAME=$(basename $docker_dir)
        KANIKO_OPTIONS="--context=$docker_dir --dockerfile=$IMAGE_DIR"

        if [[ ! -z "$CI_COMMIT_TAG" ]]; then
          KANIKO_OPTIONS="$KANIKO_OPTIONS --destination=$HARBOR_ADDRESS/$HARBOR_PROJECT/$IMAGE_NAME:$CI_COMMIT_TAG"
        fi

        if [[ "$CI_COMMIT_BRANCH" == "main" ]]; then
          KANIKO_OPTIONS="$KANIKO_OPTIONS --destination=$HARBOR_ADDRESS/$HARBOR_PROJECT/$IMAGE_NAME:latest"
        fi

        echo "Building and pushing $IMAGE_NAME..."
        /kaniko/executor $KANIKO_OPTIONS 
      done
  only:
    - tags
    - main


image: alpine/helm:3.11.1

# Job for updating charts dependencies and packing it into .tgz
build_helm:
  stage: build-helm-charts

  # This script iterates through the /helm repo 
  # On all repos the dependencies get updated
  # Then it packages the chart into .tgz 
  # To access those built charts in the next job, we declare them as artifacts, so we can "download"/access them
  script:
    - |
      for chart_dir in $CI_PROJECT_DIR/helm/*; do
        echo "Building '$chart_dir'..."
        helm dependency update "$chart_dir"
        helm package "$chart_dir"
      done
  artifacts:
    paths:
      - "$CI_PROJECT_DIR/*.tgz"
  only:
    - main

# Job for pushing the built helm charts of 'build_helm' to harbor
push_helm:
  stage: push-helm-charts

  # This script installs the cm-push command 
  # The credentials are stored in the runner and passed into the variables
  # We are adding a library (our harbor) with the credentials
  # The loop is iterating through all *.tgz files that were created in the 'build_helm' and pushing them to harbor
  script:
    - helm plugin install https://github.com/chartmuseum/helm-push.git
    - |
      HARBOR_USERNAME=$(cat /run/secrets/harbor-credentials/username)
      HARBOR_PASSWORD=$(cat /run/secrets/harbor-credentials/password)
    - helm repo add library "https://$HARBOR_ADDRESS/chartrepo/$HARBOR_PROJECT" --username "$HARBOR_USERNAME" --password "$HARBOR_PASSWORD"
    - |
      for chart_file in $CI_PROJECT_DIR/*.tgz; do
        echo "Pushing '$chart_file'..."
        helm cm-push "$chart_file" library
      done
  dependencies:
    - build_helm
  only:
    - main
