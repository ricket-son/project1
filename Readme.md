# Project 1
## Table of contents

- [What does this repository include?](#what-does-this-repository-include)
- [API](#api)
- [Database](#database)
- [Ingress Controller](#ingress-controller)
- [Logging / Monitoring](#logging--monitoring)
- [GitLab Runners](#gitlab-runners)
- [fluxCD](#fluxcd)
## Contributors:
David Gunkelmann, Johannes Gärtner, Erik Kaisler

## What does this repository include?
This repository includes all the helm charts, docker files and single manifests to set up a cluster for an imaginary veterinary company. <br/>
***List of parts:***
- database to fill with owners and pets, which will automatically be seeded on first install with the correct tables and database structure through a job,
- an api to be able to talk to the database,
- a setup to collect and visualize logs and metrics,
- a setup for a nginx ingress controller, allowing easy outside access to the relevant components,
- GitLab runners, to automate building of images and updating repositories
- fluxCD to automate deployment of the app

### Repository Structure
Built with [Mermaid.live](https://mermaid.live)

[![](https://mermaid.ink/img/pako:eNpVj89qwzAMxl8l6NywexiFptkeYO3NzkGxlcTEf4KtUErou9czYTBdJP30fRLaQQVN0MBow0PNGLm6d9JXOS7ih9aQDIf47Ku6PletmMm6_hgXdBU6qIXiP_gtHHozUuJ08LbwLnPjP4f4ccbVlKyRccBEpbFhmoyf6sSolsN5Lc4v8WegETfL9ZAl5HUPJ3AU816dn9h_PRJ4JkcSmlxqjIsE6V9ZhxuH29MraDhudIJtzdepMzhFdNCMaBO93gURWCk?type=png)](https://mermaid.live/edit#pako:eNpVj89qwzAMxl8l6NywexiFptkeYO3NzkGxlcTEf4KtUErou9czYTBdJP30fRLaQQVN0MBow0PNGLm6d9JXOS7ih9aQDIf47Ku6PletmMm6_hgXdBU6qIXiP_gtHHozUuJ08LbwLnPjP4f4ccbVlKyRccBEpbFhmoyf6sSolsN5Lc4v8WegETfL9ZAl5HUPJ3AU816dn9h_PRJ4JkcSmlxqjIsE6V9ZhxuH29MraDhudIJtzdepMzhFdNCMaBO93gURWCk)

The repository consists of 3 main folders called helm, docker and manifests respectively, which in turn hold helm charts, files to build docker images and manifest/values.yaml files for helm charts which don't require us to modify more then the values.yaml for the respective chart.

For more information regarding the charts, docker images and manifests, you can find detailed readme files in the respective folders.

# [API](helm/api/README.md)
The api runs on the flask framework and allows us to make calls to the database (GET, POST & DELETE). See the following lists of endpoints and their use.
## Owners
```h
# GET "/owners"
-> result: All owners

# GET "/ownerswithpets"
-> result: All owners included with owners pets

# GET "/owners/{ownerId}"
-> result: Owner with owner_id ownerId

# GET "/ownerswithpets/{ownerId}"
-> result: Owner with owner_id ownerId included with owners pets

# POST "/owners"
-> requests: owner model in json
			
# DELETE "/owners/{ownerId}"
```

## Pets
```h
# GET "/pets"
-> result: All pets

# GET "/pets/{petId}"
-> result: Pet with pet_id petId

# POST "/pets"
-> requests: pet model in json

# DELETE "/pets/{petId}"

Importing legacy data

# POST "/customers"
-> requests: model with owner model data (and pet model data)
```
## Responses
```json
# GET-RESPONSE
	{'status': '200', 'description': 'Success', 'data': result}
    
# POST-RESPONSE
	{'status': '201', 'description': 'Success', 'inserted_id': inserted_id}
	{'status': '400', 'description': 'Bad Request'}
	
# DELETE-RESPONSE
	{'status': '200', 'description': 'Success', 'removed_id': id}
	{'status': '400', 'description': 'Bad Request'}
	{'status': '404', 'description': 'Data not found'}
	
# IMPORT
	{'status': '201', 'description': 'Success', 'inserted': { 'owners': owner_count, 'pets': pet_count }}
```

# [Database](helm/database/README.md) 
The following models are used in the creation of the database, being injected into the pod on first startup, using a job. We are using an unmodified mariadb image found on [docker hub](https://hub.docker.com/_/mariadb).

## Tables
```sql
# OWNER
	owner_id int NOT NULL AUTO_INCREMENT,
	first_name varchar(64) NOT NULL,
	last_name varchar(64) NOT NULL,
	email varchar(64) NOT NULL,
	PRIMARY KEY (owner_id)
```
```sql
# PET
	pet_id int NOT NULL AUTO_INCREMENT,
	fk_owned_by int,
	pet_name varchar(64) NOT NULL,
	species varchar(64) NOT NULL,
	PRIMARY KEY (pet_id),
	FOREIGN KEY (fk_owned_by)
		REFERENCES owners(owner_id)
		ON DELETE CASCADE
```
# [Ingress Controller](manifests/ingress-controller/README.md)
We are running the community-maintained version of the nginx ingress controller found [here](https://github.com/kubernetes/ingress-nginx) with minimal changes to add a default backend to it. Those changes can be found in [manifests/ingress-controller/values.yaml](manifests/ingress-controller/values.yaml#L725).

## Default backend
The default backend is a small [unpriviledged nginx](https://github.com/nginxinc/docker-nginx-unprivileged) [image](https://hub.docker.com/r/nginxinc/nginx-unprivileged) serving a static 404 page we build - the code for it can be found [here](docker/default-backend/Error404/).

# [Logging](helm/logging-stack/README.md) / [Monitoring](manifests/monitoring/README.md)
## Logging
Logging is done using a FluentD [DaemonSet](helm/logging-stack/templates/fluentd-ds.yaml) and sidecars as the aggregators, an ElasticSearch [StatefulSet](helm/logging-stack/templates/es-sts.yaml) to retrieve, index and store the logs, and a Kibana [Deployment](helm/logging-stack/templates/kibana-deployment.yaml) to visualise the logs. An example for a sidecar can be found [here](helm/database/templates/deployment.yaml#L59)

## Monitoring
Monitoring of the entire cluster can be achieved with the [kube-prometheus-stack](https://artifacthub.io/packages/helm/prometheus-community/kube-prometheus-stack) with few changes to the default besides setting the [ingress](manifests/monitoring/values.yaml) of the chart. The default helm chart already comes with an assortment of useful dashboard geard towards monitoring a cluster on different levels.

# [GitLab Runners](manifests/gitlab-runner/README.md)

## Set up GitLab Runner on cluster

We'll need a secret with the GitLab runner registration token, which you can find at **"Group -> CI/CD -> register a group runner"**.

	helm repo add gitlab http://charts.gitlab.io/
	helm install gitlab-runner gitlab/gitlab-runner -f values.yaml -n gitlab-runner

> Check if the runner was registered (at the same section where you found the runner registration token).

## Using the GitLab CI 

To use the pipeline to build and push our container images and helm charts, we need to create some secrets.

We'll need a secret with the harbor-credentials (username and password) and one with the authentication for harbor (a docker-registry).
Create them in the same namespace as the gitlab runner.
These secrets will be mounted in the executing runners to be able to push to our harbor registry.

There are also some environment variables declared in the runners config (in the values.yaml). You can also change them in the gitlab-ci.yml directly. <br>**Think about** changing your secrets, too, when you want to change the server url or user that should be used.

To build our container images, we use [Kaniko](https://github.com/GoogleContainerTools/kaniko).<br>
To build our Helm Charts, we use [alpine/helm](https://hub.docker.com/r/alpine/helm).

# [fluxCD](manifests/fluxCD/README.md)

To always keep our app up to date we are using Flux. To install Flux on the cluster, you would need to get the flux cli.

**Installing flux CLI**

	curl -s https://fluxcd.io/install.sh | sudo bash


**Installation of flux**

	flux bootstrap gitlab \
	--owner <PROJECT_OWNER> \
	--repository <NAME_FOR_STORING_FLUX_DATA> \
	--branch <BRANCH_TO_ADD_REPO> \
	--path <PATH_ON_MAIN_REPO> \
	--hostname <GITLAB_URL>

With that, we'll get a directory created in our repo with some configs needed for Flux.

**Installing our app with flux**

To install our app we created a [helm chart](helm/main) with all the objects needed to create a source for our helm charts and automatically deploying them on updates.

In this helm chart you'll see some HelmRepository objects, which declare the source, where we get our helm charts from. <br>
The HelmRelease objects declare the "real" release. We can there say which chart we want, which versions, where to deploy the chart, we can also modify the values, etc. So with that the chart gets created at the end.

When the cluster is finally set up with the ingress controller, gitlab runners, you only need to install the helm chart.

	helm install <RELEASE_NAME> helm/main -n <TARGET_NAMESPACE>

That's it. This will create the flux objects, connect to our repositories, pull the charts and install them. And it will also look for any updates if we configured that ofc.
