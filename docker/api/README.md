# docker-api

**Introduction**

This API is built with flask and is used to fetch data out of a MariaDB. In general this API is used for our Kubernetes application, but can simply be bound to any other application with a MariaDB. 

This docker image is used in the helm chart [/helm/api](project1/helm/api) for deploying the application in Kubernetes.

**Environment variables of the api**

To connect the API with the MariaDB

- **DB_HOST** for the URL of the database
- **DB_PORT** the port of the database
- **DB_NAME** the name of the database to use
- **DB_USER** the user to use in the database
- **DB_PASSWORD** the user's password

**Build**

This image is built with the python-alpine image which is a small image that is already configured for python. It uses a multi-stage build to decrease the size of the final container image.
