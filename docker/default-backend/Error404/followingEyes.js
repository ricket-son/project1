const pupils = document.querySelectorAll(".ghost_eye .ghost_eye_pupil");
window.addEventListener("mousemove", (e) => {
  pupils.forEach((ghost_eye_pupil) => {
    // get x and y postion of cursor
    var rect = ghost_eye_pupil.getBoundingClientRect();
    var x = (e.pageX - rect.left) / 35 + "px";
    var y = (e.pageY - rect.top) / 25 + "px";
    ghost_eye_pupil.style.transform = "translate3d(" + x + "," + y + ", 0px)";
  });
});
