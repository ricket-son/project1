# docker-default-backend

Just a nginx image which holds our default backend.

Created with: `docker build -t project1/defaultbackend:latest .` , this container hold the files located in the "Error404" folder to be used as the default backend for our ingress controller.

## Image
The image used in the [Dockerfile](Dockerfile) is a version of the default nginx image but able to run as unprivileged and non-root user (found [here](https://hub.docker.com/r/nginxinc/nginx-unprivileged)). This is done so we have the smallest attack surface possible for the image.

Since the ingress controller comes with a default health check, we also need to add that path to the [nginx.conf](nginx.conf) to prevent the container being constantly restarted because the controller can't verify the pod to be running.

## 404 Page

The webpage we build it mostly CSS and a few lines of JavaScript, the HTML file is "just" to allign everything correctly. Check the code out in the 404Error folder if you want.