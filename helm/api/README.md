# helm-api

**Introduction**

This repository is used for the api helm chart that is using the [/docker/api](project1/docker/api) image.

We added a fluentd sidecar to this project to catch the logs and pass them to elasticsearch.

**Environment variables of the api**

These variables have to be passed in the environment to connect this api with the database.

- **DB_HOST** for the URL of the database
- **DB_PORT** the port of the database
- **DB_NAME** the name of the database to use
- **DB_USER** the user to use in the database
- **DB_PASSWORD** the user's password

**Installation**

    helm install [RELEASE_NAME] .

## API Structure / Endpoints

### Owners

    # GET "/owners"
    -> result: All owners

    # GET "/ownerswithpets"
    -> result: All owners included with owners pets

    # GET "/owners/{ownerId}"
    -> result: Owner with owner_id ownerId

    # GET "/ownerswithpets/{ownerId}"
    -> result: Owner with owner_id ownerId included with owners pets

    # POST "/owners"
    -> requests: owner model in json

    # DELETE "/owners/{ownerId}"

### Pets

    # GET "/pets"
    -> result: All pets

    # GET "/pets/{petId}"
    -> result: Pet with pet_id petId

    # POST "/pets"
    -> requests: pet model in json

    # DELETE "/pets/{petId}"

### Importing legacy data

    # POST "/customers"
    -> requests: model with owner model data (and pet model data)

### Responses

    # GET-RESPONSE
    	{'status': '200', 'description': 'Success', 'data': result}

    # POST-RESPONSE
    	{'status': '201', 'description': 'Success', 'inserted_id': inserted_id}
    	{'status': '400', 'description': 'Bad Request'}

    # DELETE-RESPONSE
    	{'status': '200', 'description': 'Success', 'removed_id': id}
    	{'status': '400', 'description': 'Bad Request'}
    	{'status': '404', 'description': 'Data not found'}

    # IMPORT
    	{'status': '201', 'description': 'Success', 'inserted': { 'owners': owner_count, 'pets': pet_count }}

### Models

    # OWNER
    	owner_id int NOT NULL AUTO_INCREMENT,
    	first_name varchar(64) NOT NULL,
    	last_name varchar(64) NOT NULL,
    	email varchar(64) NOT NULL,
    	PRIMARY KEY (owner_id)


    # PET
    	pet_id int NOT NULL AUTO_INCREMENT,
    	fk_owned_by int,
    	pet_name varchar(64) NOT NULL,
    	species varchar(64) NOT NULL,
    	PRIMARY KEY (pet_id),
    	FOREIGN KEY (fk_owned_by)
    		REFERENCES owners(owner_id)
    		ON DELETE CASCADE
