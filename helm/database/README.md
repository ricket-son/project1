# helm-database

## Introduction

This is our database helm chart, that uses the mariadb. This chart is constructed so that the schema we need for our application is inserted automatically when the chart is installed. A job is triggered, which applies the schema when it does not exist.

We also added a fluentD sidecar to the deployment to collect the logs and pass them to our elasticsearch stateful set.

## Installation

Inside database folder:

    helm install <RELEASE_NAME> .

## Configuration

In the [values.yaml](/helm/database/values.yaml) you can configure the database to your needs. To only try out the database, you don't need to configure anything. With the default settings a mariadb gets created with the image version "jammy". The schema gets inserted, like said before, through a job, that waits, till the database is ready for connections. After it checks if the schema already exists, if not, inserts it.

There is also a PVC added for persistency. The PVC is created for our cluster, if you are applying this chart on another cluster, you would need to configure the PVC.

It is recommended to change the database credentials, too. They are also easily configurable in the values.yaml. If you don't want any logging or persistency, you can also disable it in the values.
