# helm - logging - stack

The logging stack consists of 3 parts, a log aggregator (in our case [FluentD](https://www.fluentd.org/)), a service for storing and indexing the logs (in our case [ElasticSearch](https://www.elastic.co/elasticsearch/)) as well as a dashboard to get an overview of the data collected in a compact format (we use [Kibana](https://www.elastic.co/kibana/)).

## FluentD

FluentD is a log aggregator which we deploy as a daemonset to make sure at least one pod runs on every node of the cluster. This way we get any logs that are natively integrated with systemd/journald as well as general logs from the nodes - any application using journald as their default logging service also is included. For any application which doesn't natively use journald we supply the pod with a sidecar to extract the logs and stream them directly to the ElasticSearch StatefulSet. 

## ElasticSearch

ElasticSearch is our StatefulSet - it collects all the logs in one place and indexes them. We are running 3 StS with 1GB storage each, this can be scaled horizontaly with ease. 

## Kibana

Kibana is our Dashboard to be able to look at the accumulated logs - we can search for specific things like namespaces, pods or even images, get everything from info to error logs and we can even build our own dashboards if we want to.

# Installation
Installation is quite easy, download the repository, navigate to the sub-folder "helm/logging-stack" and use helm to install the chart. <br/>**NOTE:** _This installs "just" a daemonset to monitor the overall cluster, any logging that needs to happen as a sidecar will have to be added to the corresponding deployment!_

```bash
$ helm install logging-stack .
```
The default set namespace for the chart is "logging" and can be changed in the [values.yaml](values.yaml).

## Uninstalling the chart
The chart simply can be uninstalled using `helm uninstall logging-stack -n logging` (as long as you didn't change the default namespace).
Feel free to delete the namespace as well using `kubectl delete namespace logging`.

# Sidecars
For some applications we have to use sidecars to collect the logs instead of relying on the daemonset installed with this helm chart, see for example the [database deployment](../database/templates/deployment.yaml) - here we use a mysql internal feature to write the log to a specific file, share this file with the fluentd sidecar to then be read and sent to the ElasticSearch StatefulSet. Since we don't want to preserve the logs longer then the existance of the pod itself, we use an emptyDir which will be deleted as soon as the pod stops existing. The logs will still be accessible through Kibana/ElasticSearch, but won't take up more space then necessary on the rest of the cluster.
