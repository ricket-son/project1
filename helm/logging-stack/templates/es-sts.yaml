apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: es-cluster
  namespace: {{ .Values.namespace }}
spec:
  serviceName: {{ .Values.elasticSearch.appName }}
  replicas: {{ .Values.elasticSearch.replicas }}
  selector:
    matchLabels:
      app: {{ .Values.elasticSearch.appName }}
  template:
    metadata:
      labels:
        app: {{ .Values.elasticSearch.appName }}
    spec:
      containers:
      - name: {{ .Values.elasticSearch.appName }}
        image: {{ .Values.elasticSearch.image }}
        resources:
            limits:
              cpu: {{.Values.elasticSearch.resources.limits.cpu }}
            requests:
              cpu: {{ .Values.elasticSearch.resources.requests.cpu }}
        ports:
        - containerPort: {{ .Values.elasticSearch.ports.main }}
          name: rest
          protocol: TCP
        - containerPort: {{ .Values.elasticSearch.ports.interNode }}
          name: inter-node
          protocol: TCP
        volumeMounts:
        - name: data
          mountPath: /usr/share/elasticsearch/data
        env:
          - name: cluster.name
            value: k8s-logs
          - name: node.name
            valueFrom:
              fieldRef:
                fieldPath: metadata.name
          - name: discovery.seed_hosts
            value: "es-cluster-0.elasticsearch,es-cluster-1.elasticsearch,es-cluster-2.elasticsearch"
          - name: cluster.initial_master_nodes
            value: "es-cluster-0,es-cluster-1,es-cluster-2"
          - name: ES_JAVA_OPTS
            value: "-Xms512m -Xmx512m"
      initContainers:
      - name: fix-permissions
        image: busybox
        command: ["sh", "-c", "chown -R 1000:1000 /usr/share/elasticsearch/data"]
        securityContext:
          privileged: true
        volumeMounts:
        - name: data
          mountPath: /usr/share/elasticsearch/data
      - name: increase-vm-max-map
        image: busybox
        command: ["sysctl", "-w", "vm.max_map_count=262144"]
        securityContext:
          privileged: true
      - name: increase-fd-ulimit
        image: busybox
        command: ["sh", "-c", "ulimit -n 65536"]
        securityContext:
          privileged: true
  volumeClaimTemplates:
  - metadata:
      name: data
      namespace: {{ .Values.namespace }}
      labels:
        app: {{ .Values.elasticSearch.appName }}
    spec:
      volumeMode: Filesystem
      accessModes: [ "ReadWriteOnce" ]
      storageClassName: "{{ .Values.elasticSearch.storage.storageClass }}"
      resources:
        requests:
          storage: {{ .Values.elasticSearch.storage.size }}