# Petstore-App

## Introduction

This Helm Chart is the main chart of our Kubernetes application. This chart includes some HelmRelease objects that are used with FluxCD. With that, we can deploy new versions / updates continuously during runtime of the app.

This chart includes the [API chart](/helm/api), [Database chart](/helm/database) and the [Logging chart](/helm/logging). Those are all components we need to run our application. Besides there are also some secrets that are used to authenticate to our private Harbor server that we use as private registry for our container images and helm charts.

## Requirements

This chart assumes that the cluster is already set up with the **Kubernetes Nginx Ingress Controller** and the **GitLab Runner**. We excluded it from the main chart, but you could also include it easily.

## Installation

Before installing the Helm Chart, we need to change some configurations.<br>
Actually you would just need to insert your harbor credentials. 

**Installing it from the root path**
	
	helm install <RELEASE_NAME> helm/main -n <NAMESPACE> --create-namespace

**Installing it from Harbor**

> Add the Harbor repo to your Helm repos

	helm repo add <REPO_NAME> \
	--username <HARBOR_USERNAME> \
	--password <HARBOR_PASSWORD> \
	https://harbor.dieunkrauts.de/chartrepo/project1

> Install the main chart out of the Helm repo

	helm install <RELEASE_NAME> <HELM_REPO>/petstore-app -n <NAMESPACE> --create-namespace --version <CHART_VERSION>

This will install the secrets for the authentication to Harbor and all the charts needed for our application.
For more details, you can look into the specific helm charts.

## Configuration

All the configuration of the main chart is used. If you want to change anything, just modify the [values.yaml](/helm/main/values.yaml). If you want to add some configuration options to specific parts of the app, you can modify the [template](/helm/main/templates) of the HelmRelease.

For now we just added some basic configuration in the values for our needs, but like said before you can simply add more configuration to it.
