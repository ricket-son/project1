# FluxCD

## Introduction

FluxCD is a simple and lightweight open-source tool for automating the deployment of Kubernetes applications. It provides an easy-to-use interface and integrates well with Git providers and Helm, making it a popular choice for continuous deployment in Kubernetes-based environments.

## Quickstart

### Installing flux CLI

	curl -s https://fluxcd.io/install.sh | sudo bash


### Installation of flux 'in our GitLab'

**Creating the repo for storing flux data of our project**

	flux bootstrap gitlab --owner <PROJECT_OWNER> -- repository <NAME_FOR_STORING_FLUX_DATA> --branch <BRANCH_TO_ADD_REPO> --path <PATH_ON_MAIN_REPO> --hostname <GITLAB_URL>

**Could look like this**

	flux bootstrap gitlab --owner project1 --repository fluxtest --branch development --path ./clusters --hostname gitlab.dieunkrauts.de

> This will set some files in the PROJECT/clusters/fluxtest up for the use of FluxCD.

### Creating an auth secret for flux

You can create the secret through a manifest or by using the flux CLI.

	flux create secret helm <SECRET_NAME> --username <HARBOR_USERNAME> --password <HARBOR_PASSWORD> 

You can also use the `--export` option to get this as manifest output.

**Example**

	apiVersion: v1
	kind: Secret
	metadata:
		name: sample-secret
		namespace: flux-system
	stringData:
		password: secretPass
		username: meTheUser

### Adding a source repository (HelmRepository)

The HelmRepository describes the source, where flux should look up for in our case - Helm Charts. 

Again, you can simply use the flux CLI to create a source.

	flux create source helm <SOURCE_NAME> --url <SOURCE_URL> --interval <CHECK_INTERVAL> 

You can also add the `--export` here again, ofc.

**Example**

	apiVersion: source.toolkit.fluxcd.io/v1beta2
	kind: HelmRepository
	metadata:
		name: sample-repo
		namespace: flux-system
	spec:
		interval: 1m
		url: https://harbor.dieunkrauts.de/chartrepo/project1
		secretRef:
			name: sample-secret

> To check if the source is configured the right way and that we have access to it, you can use <br> `flux get sources helm` 

With that, we already have a connection to our Helm Charts on our Harbor server.

### Creating a release (HelmRelease)

The HelmRelease is the actual deployment. It checks the source we provide and installs the Helm Chart. It will keep an eye on updates if desired and updates it automatically.

Here are the examples with flux CLI and the output.

**Example in flux CLI**

	flux create helmrelease sample-release 
	--namespace=flux-system \
	--source=HelmRepository/sample-repo \
	--chart=someChartOnHarbor \
	--interval=5m \
	--release-name=sample-release-name \
	--target-namespace=sample-namespace 


**Example**

	apiVersion: helm.toolkit.fluxcd.io/v2beta1
	kind: HelmRelease
	metadata:
		name: sample-release
		namespace: flux-system
	spec:
		chart:
			spec:
				chart: someChartOnHarbor
				reconcileStrategy: ChartVersion
				sourceRef:
					kind: HelmRepository
					name: sample-repo
		interval: 5m0s
		releaseName: sample-release-name
		targetNamespace: sample-namespace

> This will use the chart "someChartOnHarbor" from our Harbor server and install it as "sample-release-name" in the namespace "sample-namespace". It will look for updates every 5 minutes and will always be up to date with the `reconcileStrategy: ChartVersion`.

### Setting values in the HelmRelease

Ofc you can also set some values in the HelmRelease.

You can either pass a values file, reference ConfigMaps, Secrets or pass values in "manually".

**Example reading from file**

	apiVersion: helm.toolkit.fluxcd.io/v2beta1
	kind: HelmRelease
	metadata:
		name: sample-release
		namespace: flux-system
	spec:
		chart:
			spec:
				chart: someChartOnHarbor
				reconcileStrategy: ChartVersion
				sourceRef:
					kind: HelmRepository
					name: sample-repo
				values:
					valueFile: /path/to/values.yaml
		interval: 5m0s
		releaseName: sample-release-name
		targetNamespace: sample-namespace


---

This is only a short part of what fluxCD can do. We can also bind it into our GitLab repository for updating our cluster resources whenever there are any code changes.


## Deploying our app

When everything is set up, we can start with deploying our application through FluxCD.

For that, we configure multiple HelmReleases.
You can see some examples of pre-defined releases that deploy our whole application.

**To deploy such HelmReleases, you simply use**

	kubectl apply -f <RELEASE_FILE>

With that, all the HelmReleases in the file get executed and deployed on the provided namespace.

**To remove them, ofc you'll use**

	kubectl delete -f <RELEASE_FILE>


### [Flux Documentation](https://fluxcd.io/flux/)
