## Introduce GitLab Runner on cluster

### Setting up GitLab Runner on cluster using the GitLab-Runner helm chart

The helm chart we are using for the GitLab Runner is ["gitlab/gitlab-runner"](https://artifacthub.io/packages/helm/gitlab/gitlab-runner).

You can easily add it to your helm repos with 

	helm repo add gitlab http://charts.gitlab.io/

To configure the runner to work with our GitLab, we create the **[secret.yaml](manifests/gitlab-runner/secret.yaml)**. For that you would need to get a registration token. You can create one to be used for the group by going to **"Group -> CI/CD -> register a group runner"**.

In the "values.yaml" we declare the URL of our GitLab instance as well as some runner variables like the secret with the registration token and some runner configs like mounting the secrets for authentication. Also we patch the ServiceAccount to be able to manage objects on the cluster.

### Installing the chart on the cluster

We'll install the runner in a separate namespace with the gitlab-runner helm chart using our custom values.yaml.

	helm install gitlab-runner gitlab/gitlab-runner -f values.yaml -n gitlab-runner

> After setting this up, we can check on our GitLab, if the runners are connected successfully. You can see it at the same path where you got the registration token.

### Triggering the runner

The runner gets triggered whenever a pipeline is triggered. It creates the necessary resources / pods, executes the pipeline and after deletes the resources.

---

## GitLab CI for building and pushing container images and helm charts
  
We are using GitLab CI for building and pushing our container images and helm charts automatically.

With the pipeline the container images & helm charts get automatically build and pushed to a Harbor registry server, from where we can simply include it on our cluster to our application.

### Jobs

**build_and_push_container_images** uses the Kaniko image to build and push our docker images to harbor.

**build_helm** & **push_helm** use the alpine/helm image to package the charts and push them into our harbor registry.

### Kubernetes Secrets

For our authentication to Harbor, we use Kubernetes secrets. They are declared in the configuration of the gitlab-runner.

**harbor-registry** is a *docker-registry* Kubernetes secret, that stores the authentication *.dockerconfigjson* variable. For the schema you can look into the **[harbor-secret.yaml](manifests/gitlab-runner/harbor-secret.yaml)** file.

	kubectl create secret docker-registry harbor-registry --docker-server <HARBOR_URL> --docker-username <USER_NAME> --docker-password <USER_PASSWORD> --docker-email <USER_EMAIL>

**harbor-credentials** is a generic Kubernetes secret that stores the credentials for harbor using a *username* & *password*. Schema is also in the **[harbor-secret.yaml](manifests/gitlab-runner/harbor-secret.yaml)** file.

	kubectl create secret generic harbor-credentials --from-literal=username=<USER_NAME> --from-literal=password=<USER_PASSWORD>

These secrets need to be created on the namespace where the GitLab-Runner is running. They will be mounted into the runners that are executing the jobs.

### ENV variables

There are also some environment variables set for configuring the url of the Harbor registry and the Harbor project which to use.

You'll find them in the [values.yaml](manifests/gitlab-runner/values.yaml) of the gitlab-runner. You can also modify them in the [.gitlab-ci.yml](.gitlab-ci.yml) and pass them in the script into the environment.

### Build-Image

To build our container images, we use [Kaniko](https://github.com/GoogleContainerTools/kaniko).

To build our Helm Charts, we use [alpine/helm](https://hub.docker.com/r/alpine/helm).
