# Ingress Controller

The Ingress Controller used in our case is the [Nginx Ingress Controller](https://kubernetes.github.io/ingress-nginx/), maintained by the k8s community - it comes with a comprehensive documentation in the form of GitHub pages and is easy to set up thanks to the use of a helm chart.

With the changes to the values.yaml we were able to add our own 404 page to the controller, running as a separate deployment.

## Installation
Installation of the chart is rather straight forward, using the following commands to first add the repo and then installing the chart using helm and the changed values.yaml
```
$ helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx 
$ helm install [RELEASE_NAME] ingress-nginx/ingress-nginx --version 4.5.2 --values values.yaml -n [CUSTOM_NAMESPACE] --create-namespace
```
These commands ensure the ingress-controller is installed in a separate namespace and uses our custom values.

Once the Controller is running and assigned an external IP (which we can check for with `kubectl get svc -n [CUSTOM_NAMESPACE]`) we can access the applications running on the cluster by using ingress resources just like this:

```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: example-ingress
  annotations:

spec:
  ingressClassName: nginx
  rules:
  - host: example.com
    http:
      paths:
      - path: /example
        pathType: Prefix
        backend:
          service:
            name: example-service
            port:
              name: example-port
```
This is just an example and needs to be modified to fit the service it's point to - in some cases you'll have to annotations to rewrite the path for example, otherwise the application won't be accessible. In other cases a small ingress like this may suffice.