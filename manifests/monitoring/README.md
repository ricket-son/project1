# kube-prometheus-stack

## Introduction

The kube-prometheus stack is a collection of Kubernetes manifests, Grafana dashboards, and Prometheus rules that are used to deploy a comprehensive monitoring solution for Kubernetes clusters. It is based on the Prometheus monitoring system and includes pre-configured dashboards and alerts that are tailored to Kubernetes environments.

### Prometheus

Prometheus is an open-source monitoring and alerting system used to collect metrics from various sources such as applications, services, and systems. 

### Grafana

Grafana is a popular open-source data visualization and monitoring tool used to create and display real-time dashboards for various data sources.

## Installation

`helm install monitoring prometheus-community/kube-prometheus-stack --values values.yaml -n monitoring`

The values.yaml enables ingress to make it accessible. A DNS for the host has to be configured, if none is available add the host and its IP to your /etc/hosts as a workaround.

## Access the Grafana dashboard

The Grafana dashboard is accessible under the following URL:

`https://project1.cn.evoila-lab.work/grafana`


### [kube-prometheus-stack helm chart](https://artifacthub.io/packages/helm/prometheus-community/kube-prometheus-stack)
