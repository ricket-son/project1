# Security concerns

## Network policies

Network policies are used to limit the ways pods can communicate with eachother - this is done in order to harden the cluster and allow for less attack surface in the case of a breach. By limiting the ways database, api and logging communicate to the absolute minimum necessary, we make sure, that even in the event of someone gaining access to a pod, they need to jump through more and more hoops to gain meaningful access to anything.

_Example network policy:_
```yaml
# A policy like this allows 2 applications to access a central application (like a database) and send requests to it.
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: abc-network-policy
  namespace: xyz
spec:
  policyTypes:
  - Ingress
  podSelector:
    matchLabels:
      app: app-1
  ingress:
  - from:
    - podSelector:
        matchLabels:
          app: app-2
  - from:
    - podSelector:
        matchLabels:
          name: app-3
```
## Image Hardening

By using multi-stage builds, as well as removing any tools which are not needed after building the application in the image, we can reduce the size, as well as the things that can be achieved when accessing the container with malicious intent.
Tools like Trivy or Harbor allow us to scan our images and eliminate any CVEs that might show up.

## Cluster Hardening

Using a CIS Benchmark we can further reduce the vulnerability of the cluster and the applications running on it.